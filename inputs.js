import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, Image, ToastAndroid, ScrollView } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Card, ListItem } from 'react-native-elements';

class Inputs extends Component {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: '#00aeef',
            headerTintColor: '#183883',
        }
    };
    state = {
        uname: '',
        password: ''
    }
    handleUser = (text) => {
        this.setState({ uname: text })
    }
    handlePassword = (text) => {
        this.setState({ password: text })
    }
    login = () => {
        const { uname, password } = this.state
        if (uname == "" & password == "") {
            ToastAndroid.show("please enter your username  and password", ToastAndroid.SHORT)
            return false
        }
        else if (uname == "") {
            ToastAndroid.show("please enter your username  and password", ToastAndroid.SHORT)
            return false
        }
        else if (password == "") {
            ToastAndroid.show("please enter your password", ToastAndroid.SHORT)
            return false
        }

        this.props.navigation.navigate('Profile', {
            name: this.state.uname
        });

    }
    render() {
        return (
            <View style={styles.container}>

                <View>
                    <Image
                        style={{ width: '100%', height: 200 }}
                        source={require('/home/jman/bankapp/barclays.png')}
                    />
                </View>
                <TextInput style={styles.input}
                    placeholder="Enter your username"
                    placeholderTextColor="#00aeef"
                    maxLength={25}
                    onChangeText={this.handleUser} />
                <TextInput style={styles.input}
                    placeholder="Enter your password"
                    placeholderTextColor="#00aeef"
                    maxLength={25}
                    secureTextEntry={true}
                    onChangeText={this.handlePassword} />

                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={
                        () => this.login(this.state.uname, this.state.password)
                    }>
                    <Text style={styles.submitButtonText}> Login </Text>
                </TouchableOpacity>
            </View>

        )
    }
}

class Details extends React.Component {

    static navigationOptions = {
        title: 'Welcome to barclays ',
        headerStyle: {
            backgroundColor: '#00aeef',
        },

        headerTintColor: 'white',

        headerTitleStyle: {
            fontWeight: 'bold',
            color: 'white'
        }

    };
    state = {
        card: ''
    }
    handleCard = (text) => {
        this.setState({ card: text })
    }
    submit = () => {

        const { card } = this.state
        if (card == "") {
            ToastAndroid.show("please enter the card Number", ToastAndroid.SHORT)
            return false
        }
        else {

        }
        this.props.navigation.navigate('Display', {
            cards: this.state.card,
            name: this.props.navigation.state.params.name
        });


    }
    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <View>
                    <Image
                        style={{ width: '100%', height: 200 }}
                        source={require('/home/jman/bankapp/barclays.png')}
                    />
                </View>
                <Text style={styles.weclome}>
                    Weclome {this.props.navigation.state.params.name} Enter your card details here
                </Text>
                <TextInput style={styles.input}

                    placeholder="Enter your 16 digit card number"
                    maxLength={12}
                    keyboardType="numeric"
                    placeholderTextColor="#00aeef"
                    onChangeText={this.handleCard} />

                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={
                        () => this.submit(this.state.card)
                    }>
                    <Text style={styles.submitButtonText}> Check transaction Details </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

class Data extends React.Component {
    static navigationOptions = {
        title: 'Payment details',
        headerStyle: {
            backgroundColor: '#00aeef',
        },

        headerTintColor: 'white',

        headerTitleStyle: {
            fontWeight: 'bold',
            color: 'white'
        }
    };

    render() {
        const { navigation } = this.props;
        const list = [
            {
                name: 'Naveen',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/check-1-512.png',
                subtitle: 'Tue 29, Oct 2019',
                amount: '₹ 500',
                status: 'Success'
            },
            {
                name: 'Arun prasath',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/wrong-3-512.png',
                subtitle: 'Mon 28, Oct 2019',
                amount: '₹ 100',
                status: 'Failed'
            },
            {
                name: 'Kumar',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/wrong-3-512.png',
                subtitle: 'Tue 29, Oct 2019',
                amount: '₹ 2000',
                status: 'Failed'
            },
            {
                name: 'Narayanan',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/check-1-512.png',
                subtitle: 'Tue 29, Oct 2019',
                amount: '₹ 3000',
                status: 'Success'
            },
            {
                name: 'Deva',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/wrong-3-512.png',
                subtitle: 'Tue 29, Oct 2019',
                amount: '₹ 200',
                status: 'Failed'
            },
            {
                name: 'Akhil',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/check-1-512.png',
                subtitle: 'Tue 29, Oct 2019',
                amount: '₹ 500',
                status: 'Success'
            },
            {
                name: 'Nivi',
                avatar_url: 'https://cdn2.iconfinder.com/data/icons/right-and-wrong/48/wrong-3-512.png',
                subtitle: 'Tue 29, Oct 2019',
                amount: '₹ 200',
                status: 'Failed'
            },

        ]
        return (
            <View style={styles.container}>

                <Card title="Card details">
                    <Text>
                        Account holder Name: {this.props.navigation.state.params.name}
                    </Text>
                    <Text>
                        card Number: {this.props.navigation.state.params.cards}
                    </Text>
                </Card>
                <Card>
                    <Text>
                        Account  ₹10000
                    </Text>
                </Card>
                <ScrollView>
                    <Card title="Transaction History">
                        {
                            list.map((l, i) => (
                                <ListItem
                                    key={i}
                                    leftAvatar={{ source: { uri: l.avatar_url } }}
                                    title={l.name}
                                    subtitle={l.subtitle}
                                    rightElement={l.amount}
                                    rightSubtitle={l.status}
                                    bottomDivider

                                />
                            ))
                        }
                    </Card>
                </ScrollView>
                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={
                        () => this.props.navigation.navigate('Home')
                    }>
                    <Text style={styles.submitButtonText}> Go home </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const MainNavigator = createStackNavigator({
    Home: { screen: Inputs },
    Profile: { screen: Details },
    Display: { screen: Data },
});

export default createAppContainer(MainNavigator);

const styles = StyleSheet.create({
    container: {
        paddingTop: 23,
        backgroundColor: 'white',


        height: '100%'
    },
    input: {
        margin: 15,
        height: 40,
        borderColor: '#00aeef',
        borderWidth: 1,
        opacity: 0.5,
        borderRadius: 15,

    },
    submitButton: {
        backgroundColor: '#00aeef',
        padding: 10,
        margin: 15,
        height: 40,
        borderRadius: 20,
    },
    submitButtonText: {
        color: 'white',
        textAlign: 'center',


    },
    textcolor: {
        textAlign: 'right',
        color: '#00aeef',
    },
    weclome: {
        textAlign: 'center',
        color: '#00aeef',

    },
})